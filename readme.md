
run docker-compose in docker
script:
  - apk add --no-cache docker-compose
  - sed "s/TAG_VERSION/${VERSION}/g" docker-compose_template.yml > docker-compose.yml
  - docker-compose up -d

or:
<!-- failed -->
 change baseImage to docker/compose

register
 sudo gitlab-runner register --docker-privileged  --docker-volumes "/certs/client"

list
sudo gitlab-runner list

unregister
sudo gitlab-runner unregister -u https://gitlab.com/ -t *****
